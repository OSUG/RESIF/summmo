#!/usr/bin/env python3
import logging
import re
from os import access, R_OK
from os.path import isfile
import paramiko
from time import gmtime, strftime
from pyzabbix import ZabbixMetric, ZabbixSender
from influxdb import InfluxDBClient

from .summer import VolumeCollection, Volume
from .functions import to_bytes

logger = logging.getLogger(name="summmo")

"""
Classe pour gérer un endpoint
"""


class SummerEndpoint:
    def __init__(self, prefix, server, username, port=22, password="", ssh_key=""):
        """
        Définition du endpoint
        prefix: Un prefixe pour le nom de tous les volumes trouvés dans ce endpoint
        uri: URI de connexion par exemple vsadmin@sum-resif-admin.ujf-grenoble.fr
        """
        self.prefix = prefix
        self.server = server
        self.port = port
        self.username = username
        self.password = password
        self.ssh_key = ssh_key
        self.client = paramiko.SSHClient()
        self.client.load_system_host_keys()
        self.client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy)
        self.volumes = VolumeCollection()
        self._fetch_volumes()

    def _fetch_volumes(self):
        if isfile(self.ssh_key) and access(self.ssh_key, R_OK):
            self.client.connect(
                self.server,
                port=self.port,
                username=self.username,
                allow_agent=False,
                key_filename=self.ssh_key,
            )
        else:
            self.client.connect(
                self.server,
                port=self.port,
                username=self.username,
                password=self.password,
                allow_agent=False,
            )
        _, ssh_stdout, _ = self.client.exec_command("volume show")
        for eline in ssh_stdout.readlines():
            if "_nfs_" in eline:
                logger.debug(eline)
                vol = Volume(eline.split(" ")[1])
                logger.debug(vol)
                self.volumes.add_volume(vol)

    def fetch_data(self):
        """
        Connexion au endpoint et récupération des informations
        Renvoie une liste d'objets SummerVolume
        """
        logger.debug("Getting the list of volumes")
        for _, vol in self.volumes.items():
            logger.debug("\nFetching %s", vol.name)
            _, ssh_stdout, _ = self.client.exec_command(f"volume show -volume {vol.id}")
            for eline in ssh_stdout.readlines():
                if re.search(r"^\s+Total User-Visible Size:", eline):
                    logger.debug(eline)
                    size_with_unit = re.split(r":\s+", eline.rstrip())[-1]
                    vol.set_value("total_size", to_bytes(size_with_unit))
                    continue
                if re.search(r"^\s+Used Size:", eline):
                    logger.debug(eline)
                    size_with_unit = re.split(r":\s+", eline.rstrip())[-1]
                    vol.set_value("used_size", to_bytes(size_with_unit))
                    continue
                if re.search(r"^\s+Available Size:", eline):
                    logger.debug(eline)
                    size_with_unit = re.split(r":\s+", eline.rstrip())[-1]
                    vol.set_value("available_size", to_bytes(size_with_unit))
                    continue
                if re.search(r"^\s+Available Snapshot Reserve Size:", eline):
                    logger.debug(eline)
                    size_with_unit = re.split(r":\s+", eline.rstrip())[-1]
                    vol.set_value("snapshot_reserve", to_bytes(size_with_unit))
                    continue
                if re.search(r"^\s+Volume Size Used by Snapshot Copies", eline):
                    logger.debug(eline)
                    size_with_unit = re.split(r":\s+", eline.rstrip())[-1]
                    vol.set_value("snapshot_size", to_bytes(size_with_unit))
                    continue
            logger.debug("Volume: %s", vol)
            _, ssh_stdout, _ = self.client.exec_command(
                f"quota report -volume { vol.id } -fields tree,disk-used,disk-limit"
            )
            # vserver    volume                     index               tree        disk-used disk-limit
            # ---------- -------------------------- ------------------- ----------- --------- ----------
            # SVMp_RESIF vol_SVMp_RESIF_nfs_scratch 2305843013508661248 webservices 588KB     400GB
            qtree_data = {}
            for eline in ssh_stdout.readlines():
                if re.search(vol.id, eline):
                    aline = re.split(r"\s+", eline.rstrip())
                    if len(aline) != 6:
                        logger.warning(f"Line {eline} has unexpected number of elements. Ignoring")
                        continue
                    qtree_data[aline[-3]] = {
                        "used": to_bytes(aline[-2]),
                        "limit": to_bytes(aline[-1]),
                    }
            logger.debug("QTREE: %s", qtree_data)
            vol.set_value("qtrees", qtree_data)

    def zabbix_lld(self):
        lld = {"data": []}
        for _, vol in self.volumes.items():
            logger.debug(vol)
            lld["data"].append(
                {"{#SUMMER_LIF}": self.prefix, "{#SUMMER_JUNCTION}": vol.name}
            )
        return lld

    def zabbix_report(self, zabbix_server, zabbix_agent):
        zabbix_packet = []
        for _, vol in self.volumes.items():
            for key, value in vol.data.items():
                zabbix_packet.append(
                    ZabbixMetric(
                        zabbix_agent,
                        f"summer.{ key }[{ self.prefix },{ vol.name }]",
                        vol.data[key],
                    )
                )
        try:
            ZabbixSender(zabbix_server=zabbix_server).send(zabbix_packet)
        except Exception as err:
            logger.error("Unexpected error writing data to Zabbix")
            logger.error(err)

    def influxdb_report(
        self,
        influxdb_host,
        influxdb_port,
        influxdb_database,
        influxdb_user,
        influxdb_password,
        influxdb_measurement,
        influxdb_rp,
    ):
        logger.debug("Reporting JSON data to influxdb")
        influxdb_json_data = []
        for _, vol in self.volumes.items():
            influxdb_json_data.append(
                {
                    "measurement": influxdb_measurement,
                    "tags": {"lif": self.prefix, "volume": vol.name, "qtree": None},
                    "time": strftime("%Y-%m-%dT%H:%M:%SZ", gmtime()),
                    "fields": {
                        "available": vol.data["available_size"],
                        "used": vol.data["used_size"],
                        "total": vol.data["total_size"],
                        "snapshot_used": vol.data["snapshot_size"],
                        "snapshot_reserve": vol.data["snapshot_reserve"],
                    },
                }
            )
            for qtree, values in vol.data["qtrees"].items():
                # On parcourt le dictionnaire des quotas
                influxdb_json_data.append(
                    {
                        "measurement": influxdb_measurement,
                        "tags": {
                            "lif": self.prefix,
                            "qtree": f"{ vol.name }::{ qtree }",
                        },
                        "time": strftime("%Y-%m-%dT%H:%M:%SZ", gmtime()),
                        "fields": values,
                    }
                )

        logger.debug(influxdb_json_data)
        try:
            logger.info("Sending data to influxdb")
            logger.debug("host     = %s", influxdb_host)
            logger.debug("port     = %s", influxdb_port)
            logger.debug("database = %s", influxdb_database)
            logger.debug("username = %s", influxdb_user)
            client = InfluxDBClient(
                host=influxdb_host,
                port=influxdb_port,
                database=influxdb_database,
                username=influxdb_user,
                password=influxdb_password,
                ssl=True,
                verify_ssl=False,
            )
            client.write_points(influxdb_json_data, retention_policy=influxdb_rp)
        except Exception as err:
            logger.error("Unexpected error writing data to influxdb")
            logger.error(err)

        return
