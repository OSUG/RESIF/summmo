#!/usr/bin/env python3
import re
import logging

logger = logging.getLogger("summmo")


def to_bytes(size):
    """
    Convert a string formated as size and unit (for instance 43.4GB) in bytes.
    Returns an integer.
    """
    if size == "-":
        return 0
    # initialize power factor
    power = 1
    # List of valid units
    valid_units = ["B", "KB", "MB", "GB", "TB"]
    # Extract unit from string
    try:
        unit = re.split("[0-9]", size)[-1]
        # Check if we have a valid unit
        if not unit in valid_units:
            raise ValueError
        # Compute the power of 1024 from the position in the valid_units list
        for position, item in enumerate(valid_units):
            if item == unit:
                power = 1024 ** position
        # Get the numeric part of size
        num = re.split("[A-Z]+", size)[0]
        # Compute as bytes and return
        return int(float(num) * power)
    except ValueError:
        logger.error("%s does not match pattern.", size)
        raise
