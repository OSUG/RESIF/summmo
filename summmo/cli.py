#!/usr/bin/env python3

import logging
import click
import click_log
import json
import re
from dotenv import load_dotenv

from .endpoint import SummerEndpoint

load_dotenv()

logger = logging.getLogger(name="summmo")
click_log.basic_config(logger)


@click.group()
@click.option(
    "--password", help="SUMMER ssh management password", envvar="SUMMMO_PASSWORD"
)
@click.option(
    "--ssh-key", help="SUMMER ssh management private key file", envvar="SUMMMO_SSH_KEY"
)
@click.option("--endpoint", help="SUMMER ssh management host", envvar="SUMMMO_ENDPOINT")
@click.option(
    "--endpoint-name",
    help="SUMMER ssh management host name. Default is the shortname of endpoint",
    envvar="SUMMMO_ENDPOINT_NAME",
)
@click.option(
    "--login",
    default="vsadmin",
    help="SUMMER ssh management login",
    envvar="SUMMMO_LOGIN",
)
@click_log.simple_verbosity_option(logger)
@click.pass_context
def cli(ctx, endpoint, endpoint_name, password, login, ssh_key):
    logger.debug(f"endpoint: {endpoint}, prefix: {endpoint_name}")
    ctx.ensure_object(dict)
    ctx.obj["password"] = password
    ctx.obj["endpoint"] = endpoint
    ctx.obj["login"] = login
    ctx.obj["ssh_key"] = ssh_key
    if not endpoint_name:
        endpoint_name = endpoint.split(".")[0]
    ctx.obj["endpoint_name"] = endpoint_name


@cli.command(help="Make Zabbix LLD data")
@click.pass_context
def lld(ctx):
    """
    Command line interface
    """
    ep = SummerEndpoint(
        ctx.obj["endpoint_name"],
        ctx.obj["endpoint"],
        username=ctx.obj["login"],
        password=ctx.obj["password"],
        ssh_key=ctx.obj["ssh_key"],
    )
    lld = ep.zabbix_lld()
    print(json.dumps(lld))


@cli.command(help="Fetch all data for all volumes and report")
@click.pass_context
@click.option(
    "--zabbix-server",
    help="Report to zabbix server",
    envvar="SUMMMO_REPORT_ZABBIX_SERVER",
)
@click.option(
    "--zabbix-agent", help="Zabbix agent name", envvar="SUMMMO_REPORT_ZABBIX_AGENT"
)
@click.option(
    "--influxdb",
    help="Report to influxdb",
    is_flag=True,
    default=False,
    envvar="SUMMMO_REPORT_INFLUXDB",
)
@click.option(
    "--influxdb-measurement",
    help="Influxdb measurement",
    envvar="SUMMMO_REPORT_INFLUXDB_MEASUREMENT",
)
@click.option(
    "--influxdb-host", help="Influxdb host", envvar="SUMMMO_REPORT_INFLUXDB_HOST"
)
@click.option(
    "--influxdb-port",
    help="Influxdb port",
    envvar="SUMMMO_REPORT_INFLUXDB_PORT",
    default=8086,
)
@click.option(
    "--influxdb-database",
    help="Influxdb database",
    envvar="SUMMMO_REPORT_INFLUXDB_DATABASE",
)
@click.option(
    "--influxdb-user", help="Influxdb user", envvar="SUMMMO_REPORT_INFLUXDB_USER"
)
@click.option(
    "--influxdb-password",
    help="Influxdb password",
    envvar="SUMMMO_REPORT_INFLUXDB_PASSWORD",
)
@click.option(
    "--influxdb-rp",
    help="Influxdb retention policy",
    envvar="SUMMMO_REPORT_INFLUXDB_RP",
)
def report(
    ctx,
    zabbix_server,
    zabbix_agent,
    influxdb,
    influxdb_measurement,
    influxdb_host,
    influxdb_port,
    influxdb_database,
    influxdb_user,
    influxdb_password,
    influxdb_rp,
):
    ep = SummerEndpoint(
        ctx.obj["endpoint_name"],
        ctx.obj["endpoint"],
        username=ctx.obj["login"],
        password=ctx.obj["password"],
        ssh_key=ctx.obj["ssh_key"],
    )
    ep.fetch_data()
    if influxdb:
        ep.influxdb_report(
            influxdb_host,
            influxdb_port,
            influxdb_database,
            influxdb_user,
            influxdb_password,
            influxdb_measurement,
            influxdb_rp,
        )
    if zabbix_server:
        logger.info(f"Sending data to Zabbix server f{zabbix_server}")
        ep.zabbix_report(zabbix_server, zabbix_agent)
    logger.info("Done")


if __name__ == "__main__":
    cli(ctx={}, auto_envvar_prefix="SUMMMO")
