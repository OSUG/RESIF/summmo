#!/usr/bin/env python3
import re
import logging


class Volume:
    """
    Classe pour les informations de volumes summer
    """

    def __init__(
        self,
        id,
        total_size=0,
        used_size=0,
        available_size=0,
        snapshot_size=0,
        snapshot_reserve=0,
    ):
        """
        Initialisation
        """
        self.id = id
        try:
            self.name = re.sub(r"^.*_nfs_", "", id)
        except:
            self.name = id
        self.data = {}
        self.data["total_size"] = total_size
        self.data["used_size"] = used_size
        self.data["available_size"] = available_size
        self.data["snapshot_size"] = snapshot_size
        self.data["snapshot_reserve"] = snapshot_reserve
        self.data["qtrees"] = {}

    def set_percent(self, attribute, percentage):
        absolute = self.data["total_size"] * percentage / 100
        self.data[attribute] = absolute

    def set_value(self, attribute, value):
        self.data[attribute] = value

    def __repr__(self):
        return f"""{self.name}:
  id: {self.id}
  total size       : {self.data['total_size']}
  used size        : {self.data['used_size']}
  available size   : {self.data['available_size']}
  snapshot size    : {self.data['snapshot_size']}
  snapshot reserve : {self.data['snapshot_reserve']}"""


class VolumeCollection:
    def __init__(self):
        """
        Initialisation
        """
        self._volumes = {}

    def add_volume(self, volume):
        """
        Add a volume in the collection.
        Overwrites any existing volume with the same name.
        """
        self._volumes[volume.name] = volume

    def items(self):
        """
        Return list of volumes
        """
        return list(self._volumes.items())
