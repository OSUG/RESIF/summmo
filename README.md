# summmo : SUMMER Monitoring

summmo is a tool to collect metrics from SUMMER SSH interface and push them in influxdb and/or Zabbix.

## Installation

  pip install summo
  
## Configuration

Recommanded configuration is to set environment variables.

You can write a `.env` file at the project root with all variables set like this:

    SUMMMO_ENDPOINT=sum-resif-admin.ujf-grenoble.fr
    SUMMMO_ENDPOINT_NAME=resif
    SUMMMO_LOGIN=vsadmin
    SUMMMO_PASSWORD=XXXXXXXX
    SUMMMO_SSH_KEY=path/to/ssh_key
    SUMMMO_REPORT_ZABBIX=True
    SUMMMO_REPORT_ZABBIX_SERVER=monitoring.osug.fr
    SUMMMO_REPORT_ZABBIX_AGENT=resif-vm44.u-ga.fr
    SUMMMO_REPORT_INFLUXDB=False
    SUMMMO_REPORT_INFLUXDB_MEASUREMENT=summervolumes
    SUMMMO_REPORT_INFLUXDB_HOST=osug-influxdb.u-ga.fr
    SUMMMO_REPORT_INFLUXDB_DATABASE=telegrafresif
    SUMMMO_REPORT_INFLUXDB_USER=resifmonitor
    SUMMMO_REPORT_INFLUXDB_PASSWORD=iiKa5GfvDzWDXyE797
    SUMMMO_REPORT_INFLUXDB_RP=forever

## Execution

  summmo --help
  
## Zabbix configuration

You can load the zabbix template provided.

For low level discovery, use:

  summmo lld
