#!/usr/bin/env python3
import pytest
import os
from summmo import endpoint


def test_summer_endpoint_connect():
    password = os.getenv('SUMMMO_PASSWORD')
    ep = endpoint.SummerEndpoint(prefix="test",server="sum-resif-admin.ujf-grenoble.fr", password=password, username="vsadmin")

def test_summer_endpoint_zabbix_lld():
    password = os.getenv('SUMMMO_PASSWORD')
    ep = endpoint.SummerEndpoint(prefix="test",server="sum-resif-admin.ujf-grenoble.fr", password=password, username="vsadmin")
    lld = ep.zabbix_lld()
    print(lld)
